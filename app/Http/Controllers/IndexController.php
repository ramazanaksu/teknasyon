<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function first_api(){
        $data = [
            'deneme1' => '1',
            'deneme2' => '2',
            'deneme3' => '3'
        ];
        return response()->json($data);
    }
}
