<?php

use App\Http\Controllers\IndexController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/user-create', function (Request $request){
    \Illuminate\Support\Facades\DB::table('devices')::create([
        'uid' => 323232,
        'appId' => 45454,
        'language' => 'turkish',
        'os' => 'os',
        'expire-date' => Carbon::today()
    ]);
});
